FROM node:14.15.3-alpine3.12

RUN apk add dumb-init

ENV NODE_ENV production

WORKDIR /usr/src/app

# RUN apt-get update

# RUN apt-get install -y cron

COPY --chown=node:node . /usr/src/app

COPY package*.json ./

RUN npm cache clean --force

RUN npm ci --only=production

USER node

COPY . /usr/src/app

EXPOSE 3001

CMD ["exec", "crond", "-f"]

# CMD ["npm", "run", "server"]

CMD [ "dumb-init", "node", "drivers/webserver/server.js" ]

# CMD [ "dumb-init", "npm", "run", "server" ]