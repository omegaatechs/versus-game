require('dotenv').config();

module.exports = {
    NODE_ENV: process.env.NODE_ENV,
    PORT: process.env.PORT,
    baseUrl: process.env.BASE_URL,
    prod: {
        mongo: {
            MONGO_USER: process.env.MONGO_USER,
            MONGO_PW: process.env.MONGO_PW,
            MONGO_DOMAIN: process.env.MONGO_DOMAIN,
            MONGO_PORT: process.env.MONGO_PORT
        }
    },
    dev: {
        mongo: {
            MONGO_USER: process.env.MONGO_USER,
            MONGO_PW: process.env.MONGO_PW,
            MONGO_DOMAIN: process.env.MONGO_DOMAIN
        }
    },
    game: {
        id: process.env.GAMEID,
        room: {
            MAX_USERS: process.env.MAX_USERS_PER_ROOM
        }
    },
    otherGameProps: [{
        id: process.env.OTHER_GAME_PROPS_ID_11,
        betAmount: process.env.OTHER_GAME_PROPS_BETAMOUNT_11,
        percentageCut: process.env.OTHER_GAME_PROPS_PERCENTAGECUT_11,
        gameDrawPercentageCut: process.env.OTHER_GAME_PROPS_GAME_DRAW_CUT_11
    },
    {
        id: process.env.OTHER_GAME_PROPS_ID_12,
        betAmount: process.env.OTHER_GAME_PROPS_BETAMOUNT_12,
        percentageCut: process.env.OTHER_GAME_PROPS_PERCENTAGECUT_12,
        gameDrawPercentageCut: process.env.OTHER_GAME_PROPS_GAME_DRAW_CUT_12
    }],
    microServices: {
        coins: process.env.MICROSERVICES_COINSURL,
        coinsMasterId: process.env.MICROSERVICES_COINS_MASTERID,
        submission: process.env.MICROSERVICES_SUBMISSIONURL
    },
    serviceInstanceId: process.env.SERVICE_INSTANCEID
}