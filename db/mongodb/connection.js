let config = require('../../config')
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);
// mongoose.set('useMongoClient', true);
var path = require('path');
fs = require('fs');

//Specify the Amazon DocumentDB cert
var ca = [fs.readFileSync(path.join(__dirname, '/rds-combined-ca-bundle.pem'), 'utf8')];

let env = process.env.NODE_ENV;
if (env === 'production') {

    // const username = config.prod.mongo.MONGO_USER
    // const password = config.prod.mongo.MONGO_PW
    // mongoose.connect(`mongodb://${username}:${password}@localhost:27017/rooms`)
    console.log("in production")

    // console.log(`mongodb://${config.prod.mongo.MONGO_USER}:${config.prod.mongo.MONGO_PW}@${config.prod.mongo.MONGO_DOMAIN}:${config.prod.mongo.MONGO_PORT}/rooms?ssl=true&ssl_ca_certs=rds-combined-ca-bundle.pem&retryWrites=false`)
    mongoose.connect(`mongodb://${config.prod.mongo.MONGO_USER}:${config.prod.mongo.MONGO_PW}@${config.prod.mongo.MONGO_DOMAIN}:${config.prod.mongo.MONGO_PORT}/rooms?ssl=true&ssl_ca_certs=rds-combined-ca-bundle.pem&replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=false`, {
        useCreateIndex: true,
        useFindAndModify: false,
        sslValidate: true,
        sslCA: ca
    })
        .then(() => console.log("connected successfully"))
        .catch((err) => console.log("mongoose error", err))


} else if (env === 'dev') {
    try {
        const username = config.dev.mongo.MONGO_USER
        const password = config.dev.mongo.MONGO_PW
        const domain = config.dev.mongo.MONGO_DOMAIN
        console.log(`mongodb://${username}:${password}@${domain}:27017/rooms`)
        mongoose.connect(`mongodb://${username}:${password}@${domain}:27017/rooms`)
    }
    catch (e) {
        console.log("mongo connection error")
    }

} else {
    mongoose.connect('mongodb://localhost:27017/rooms')

}

/* var connectWithRetry = function() {
    
    return mongoose.connection.once('open', function () {
        console.log('Connection has been made');
    }).on('error', function (error) {
        console.log("mongo connection error in connection once");
        setTimeout(connectWithRetry, 5000);
    }).on('disconnected', function () {
        console.log('Connection disconnected');
    })
  };
  connectWithRetry(); */

// Signal connection
mongoose.connection.once('open', function () {
    console.log('Connection has been made');
}).on('error', function (error) {
    console.log("mongo connection error in connection once");
}).on('disconnected', function () {
    console.log('Connection disconnected');
})

module.exports = mongoose