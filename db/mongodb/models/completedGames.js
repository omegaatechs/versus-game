let mongoose = require('../connection')

let Schema = mongoose.Schema
let completedGamesSchema = new Schema({
    _id: String,
    socketUserMap: Array,
    gameId: Number,
    userWon: String,
    status: String,
    problemId: Number,
    time: Object
})

let completedGames = mongoose.model('completedGames', completedGamesSchema)

module.exports = completedGames