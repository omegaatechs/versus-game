let mongoose = require('../connection')

let Schema = mongoose.Schema
let RoomsSchema = new Schema({
    _id: String,
    socketUserMap: Array,
    gameId: Number,
    userWon: String,
    status: String,
    problemId: Number,
    time: Object
})

let Room = mongoose.model('Room', RoomsSchema)

module.exports = Room