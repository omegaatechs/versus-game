let mongoose = require('../connection')

let Schema = mongoose.Schema
let hangedUserSchema = new Schema({
    _id: String,
    prevRoom: String
})

let hangedUser = mongoose.model('hangedUser', hangedUserSchema)

module.exports = hangedUser