
let buildMakeRoom = function (roomValidator) {

    return ({
        roomId,
        socketUserMap,
        gameId,
        status
    } = {}) => {
        let { error } = roomValidator({
            roomId,
            socketUserMap,
            gameId,
            status
        })

        if (error) throw new Error(error)
        return {
            roomId,
            socketUserMap,
            gameId,
            status
        }
    }
}
module.exports = buildMakeRoom