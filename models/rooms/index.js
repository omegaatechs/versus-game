let buildMakeRoom = require('./rooms')
let roomSchema = require('./rooms-schema')
let roomValidator = require('../validator/')(roomSchema)
let makeRoom = buildMakeRoom(roomValidator)

module.exports = makeRoom
