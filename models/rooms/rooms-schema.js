let Joi = require('joi')

// module.exports = Joi.object().keys({
//     roomId: Joi.string().required().error(() => 'must have name as string'),
//     gameId: Joi.string().error(() => 'Should be string'),
//     socketIds: Joi.object().error(() => 'Should be object'),
// })

let SocketUser = Joi.object().keys({
    _id: Joi.string().required(),
    uId: Joi.string().required(),
    status: Joi.string().required()
})


module.exports = Joi.object().keys({
    roomId: Joi.string().required(),
    socketUserMap: Joi.array().items(SocketUser),
    gameId: Joi.number(),
    status: Joi.string().required()
})
