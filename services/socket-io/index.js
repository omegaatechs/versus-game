/*

TODO:
  ! [x] Restrict one socket per userID
  ! [] Add models for HangedUser collections and clean up data-access layer
  ! [] Fix routes goto server.js and check the bottom for existing routes
  ! [] Fix markdown renderer
  ! [] Auto-check whether mongo is available when server started, throw error if not present and run db-init.js if needed [include in dockerfile]
  ! [] Check redis adapter for socketIO for storing io variables and making data on server persistent
    Test scenario:
        a. Run server and client
        b. Connection then and allocate problem
        c. Cut the server
        d. Turn it on again
        e. Check if it remembers the rooms
  * [] Handle timer
  * [] Take performance test on ec2
  * [] Find the diff between polling and websocket transport difference

*/

//Config
const config = require('../../config/index');

//Problems
const problemServices = require('../problem/index');

//Event
const EventEmitter = require('events');
const afterSocketEmitter = new EventEmitter();

//DB functions
const dataFunctions = require('../../data-access/room-db/mongodb/index');
const { v4: uuidv4 } = require('uuid');


// * Local Config *//
// let room = `room-${new Date()}-`; // generate randomly or serially
let roomSize = parseInt(config.game.room.MAX_USERS);
let roomNo = uuidv4();;

// Axios
const axios = require('axios');

//Timer 
const moment = require('moment');

//Cron
const cron = require('cron');

// *** Socket functions *** //

// ** Socket Retriever ** //
let getSocketById = function getSocketById(socketId, io) {
    return io.sockets.sockets.get(socketId);
}

// ** Game connection Handler ** //
let onSocket = async function onSocket(socketId, uId, io, gameId) {
    let room = `room-` + config.serviceInstanceId.toString() + "-" + roomNo.toString();
    let socket = getSocketById(socketId, io);
    if (typeof socket !== undefined) {

        socket.join(room)

        if (io.sockets.adapter.rooms.get(room).size === roomSize) {
            roomNo = uuidv4();
        }

        if (io.sockets.adapter.rooms.get(room).size === roomSize) {
            await dataFunctions.updateUserProps(["_id"], [room], ["status"], ["RF"]).then().catch()
            afterSocketEmitter.emit('event', room, io, gameId);
        }

        let data = {
            roomId: room,
            socketUserMap: [],
            gameId: gameId,
            status: "RCO"
        }

        data.socketUserMap.push({ _id: socketId, uId: uId, status: "N" });
        if (!(io.sockets.adapter.rooms.get(room).size === 1)) {
            data.status = "RF";
            await dataFunctions.addUserToRoom(data.roomId, socketId, uId).then().catch();
        }
        else {
            await dataFunctions.createRoom(data.roomId, data.socketUserMap, data.gameId, data.status).then().catch();
        }
    }
}

let startTimer = async function (time, timeFormat, hours, res) {

    let currentTime = moment().toDate();

    var date = moment().add(time, timeFormat).toDate(); // ! change to minutes

    var mins = date.getMinutes();

    var secs = date.getSeconds();

    var hrs = date.getHours();

    var dayOfMonth = date.getDate();

    var month = date.getMonth();

    var dayOfWeek = date.getDay();
    console.log(currentTime)
    console.log(date)

    // * Attach  cloudwatch cron here instead of cronjob
    console.log(`${secs} ${mins} ${hrs} ${dayOfMonth} ${month} ${dayOfWeek}`)
    let job = new cron.CronJob(`${secs} ${mins} ${hrs} ${dayOfMonth} ${month} ${dayOfWeek}`, async () => {
        console.log('Time ended for ', res.room);
        await axios.post(config.baseUrl + "/time", { roomId: res.room }).then().catch();
    }, null, true);

    job.start();
    return { currentTime, endTime: date }
}

// ** Problem Sender ** //
let afterSocket = async function afterSocket(room, io, gameId, event) {

    if (io.sockets.adapter.rooms.get(room).size === roomSize) {

        let difficulty;
        if (gameId === 11) {
            difficulty = "easy"
        }
        else if (gameId === 12) {
            difficulty = "medium"
        }
        else if (gameId === 13) {
            difficulty = "hard"
        }

        let res = await axios.post(config.microServices.submission + "/question/get", { difficulty: difficulty }).then().catch((e) => {
            throw Error("In afterSocket Submission microservice is down")
        });

        if (res.data.length === 0) return console.error("No problem was recommended");
        res = res.data[0]
        let question = {
            id: res._id,
            name: res.name,
            description: res.description,
            languagesSupported: res.languagesSupported,
            testCasesForUser: res.testCasesForUser,
            tags: res.tags,
            likes: res.likes,
            dislikes: res.dislikes,
            difficulty: res.difficulty,
            initialCodeForUser: res.initialCodeForUser,
            time: res.time,
            timeFormat: res.timeFormat
        }

        let timer = await startTimer(res.time, res.timeFormat, "", { room: room }).then().catch();
        let currentTime = moment(timer.currentTime);
        let endTime = moment(timer.endTime);
        let timeDiff = endTime.diff(currentTime)

        let resRoom = {
            question,
            roomId: room,
            service: "versusGame",
            gameId: gameId,
            timeLeft: timeDiff,
            unit: 'MilliSeconds'
        }


        problemServices.sendProblemToRoom(resRoom, room, io);
        console.log("Room create, Game ongoing...", room)
        await dataFunctions.updateUserProps(["_id"], [room], ["status", "problemId", "gameId", "time"], ["GO", res._id, gameId, timer]).then().catch()
    }
}

let resumeGameData = async (roomInfo, io) => {
    let res = await axios.post(config.microServices.submission + "/question/get/id", { questionId: roomInfo.problemId }).then().catch((e) => {
        throw Error("In afterSocket Submission microservice is down")
    });
    res = res.data
    let currentTime = moment()//.tz('Asia/Kolkata');
    let endTime = moment(roomInfo.time.endTime)
    let timeDiff = endTime.diff(currentTime)
    let question = {
        id: res._id,
        name: res.name,
        description: res.description,
        languagesSupported: res.languagesSupported,
        testCasesForUser: res.testCasesForUser,
        tags: res.tags,
        likes: res.likes,
        dislikes: res.dislikes,
        difficulty: res.difficulty,
        initialCodeForUser: res.initialCodeForUser,
        time: res.time,
        timeFormat: res.timeFormat
    }

    let resRoom = {
        question: question,
        roomId: roomInfo._id,
        gameId: roomInfo.gameId,
        service: "versusGame",
        timeLeft: timeDiff,
        unit: 'MilliSeconds'
    }

    // console.log(resRoom)

    // let timer = await startTimer(res.time, res.timeFormat, "", { room: room }).then().catch();
    io.sockets.in(roomInfo._id).emit('resumeGameData', resRoom);

}


// ** Client Reconnection Handler ** //
let resumeGame = async (userId, socket, io) => {

    let userInfo = await dataFunctions.getUserFromHangedUsers(userId).then().catch();
    let roomInfo = await dataFunctions.getRoom(userInfo.prevRoom).then().catch();
    // console.log(roomInfo)
    if (roomInfo === null) {
        roomInfo = await dataFunctions.getCompletedGameById(userInfo.prevRoom).then().catch();
        if (roomInfo === null) return
        roomInfo = roomInfo.toJSON()
    }
    if (roomInfo.status === "GO" || roomInfo.status === "RF") {

        socket.join(userInfo.prevRoom)

        if (io.sockets.adapter.rooms.get(userInfo.prevRoom).size === roomSize) {
            // io.sockets.in(userInfo.prevRoom).emit('pending', true);
            // afterSocketEmitter.emit('event', userInfo.prevRoom, io, gameId, 'noTimer');//!
            resumeGameData(roomInfo, io)
        }

        await dataFunctions.updateUserProps(["_id", "socketUserMap.uId"], [userInfo.prevRoom, userId], ["socketUserMap.$._id", "socketUserMap.$.status"], [socket.id, "N"]).then().catch();
        await dataFunctions.deleteFromHangedUsers(userId).then().catch();
    } else {
        // room = io.sockets.adapter.rooms.get(userInfo.prevRoom)
        // console.log(userInfo.prevRoom, room)
        // roomInfo.toJSON()
        delete roomInfo['socketUserMap']
        delete roomInfo['problemId']
        delete roomInfo['__v']
        delete roomInfo['time']
        /*
        {"_id":"room-Ins1-32a80eb5-453c-4552-9535-292ec3ea58d7","gameId":0,"status":"RCO"}
        */

        socket.emit('resumeGameCompleted', roomInfo)
        // console.log(socket)
        // room.emit('reconnectGameCompleted', roomInfo)
        await dataFunctions.deleteFromHangedUsers(userId).then().catch();
    }
    // * emit problem again to socket
}

// ** Accidental server restart handler **//
// ! install redis to make socket data persistent

afterSocketEmitter.on('event', afterSocket);
module.exports = {
    onSocket,
    getSocketById,
    afterSocket,
    resumeGame
}

