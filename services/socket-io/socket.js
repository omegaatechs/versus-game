const dataFunctions = require('../../data-access/room-db/mongodb/index');
const socketFunctions = require('./index');
const gameFunctions = require("../game-logic/index")
let config = require("../../config/index");
const { default: axios } = require('axios');

module.exports = async function (io) {

    let socket = await io.on('connection', async socket => {

        if (typeof socket !== "object") {
            return new Error("Invalid socket object (typeof socket) " + typeof socket)
        }

        socket.on('uId', async (data) => {

            // * Additional prop for future use *// 
            let userId = data.userId;
            let gameId = data.gameId;
            socket.uId = userId;

            console.log(data)
           
            // console.log(await dataFunctions.getUserFromHangedUsers(userId).then())
            if (await dataFunctions.getUserFromHangedUsers(userId).then().catch()) {
                console.log("resumeGame")
                socketFunctions.resumeGame(userId, socket, io, gameId);
            }
            else {
                let res = await dataFunctions.doesUserExitsInGame(userId).then().catch();
                if (res.length !== 0) {
                    socket.emit("userStatusError", "user already connected in a room");
                    return
                } else {
                    for (let k = 0; k < config.otherGameProps.length; k++) {
                        if (gameId === parseInt(config.otherGameProps[k].id)) {
                            // let userCoins = await axios.post(config.microServices.coins + "/coins/show", { "userId": userId })
                            //     .then()
                            //     .catch((res) => { console.log("cannot connect to coins api"); socket.disconnect(); })
                            // if(userCoins === undefined) return
                            // if (userCoins !== undefined && parseInt(userCoins.data.coins) < parseInt(config.otherGameProps[k].betAmount)) {
                            //     socket.disconnect()
                            //     console.log("less coins disconnecting....")
                            //     return
                            // }
                        }
                    }
                    socketFunctions.onSocket(socket.id, userId, io, gameId);
                }

            }

        })

        socket.on('withdrawGame', async (data) => {
            await gameFunctions.withdrawEvent(data, "withdrawGame", io).then().catch()
        })

        socket.on('disconnecting', async (reason) => {
            let room = [...socket.rooms.values()][1];
            // get room details
            // if status === rco then delete the room
            if(room === undefined) return
            let roomInfo = await dataFunctions.getRoom(room).then()
            if(roomInfo === undefined || roomInfo === null) return
            if(roomInfo.status === "RCO"){
                await dataFunctions.deleteRoomById(room).then()
                console.log("RCO", roomInfo)
                return
            }
            
            //! check if the user exists
            await dataFunctions.CreateAndAddHangedUser(room, socket.id, "disconnecting").then().catch()
            await dataFunctions.updateUserProps(["_id", "socketUserMap._id"], [room, socket.id], ["socketUserMap.$._id", "socketUserMap.$.status"], ["", "H"]).then().catch();
        });

        socket.on("disconnect", () => {
            console.log("Client disconnected");
        });

    });
    return socket
};
