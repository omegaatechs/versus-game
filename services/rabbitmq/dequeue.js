// Queue
var amqp = require('amqplib/callback_api');
const CONN_URL = 'amqp://localhost';

function dequeue() {
    amqp.connect(CONN_URL, function (error0, connection) {
        if (error0) {
            throw error0;
        }
        connection.createChannel(function (error1, channel) {
            if (error1) {
                throw error1;
            }
            var queue = 'socket ID queue';
            channel.assertQueue(queue, {
                durable: false
            });
            channel.consume(queue, async function (msg) {
                console.log("Received %s", JSON.parse(msg.content.toString()));
                let out = JSON.parse(msg.content.toString());
                var funcBody = out.actions.match(/function[^{]+\{([\s\S]*)\}$/);
                console.log(out.data.userId)
                var newFunc = new Function('jsonFunc', funcBody);
                await newFunc(out.data.userId)

            }, {
                noAck: true
            });
        });
    });
}