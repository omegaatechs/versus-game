// Queue
var amqpPromise = require('amqplib');
var amqp = require('amqplib/callback_api');
const CONN_URL = 'amqp://localhost';
const dataFunctions = require('../../data-access/room-db/mongodb/index')

let enqueue = async function enqueue(msg) {

    return new Promise(async (resolve, reject) => {
        // console.log("ro00om.. ", msg.actions, msg.data.roomId)
        let connect = await conn.then()
        ch = await connect.createChannel().then();
        var queue = 'socket ID queue';
        await ch.assertQueue(queue, {
            durable: false
        }).then()

        let dup = msg
        console.log("room.. ", dup.actions, dup.data.roomId)
        msg = JSON.stringify(msg);
        ch.sendToQueue(queue, Buffer.from(msg));
        resolve()
    })
}
let dCounter = 0;

// don't use this dequeue
let dequeue = function dequeue() {
    return new Promise((resolve, reject) => {
        amqp.connect(CONN_URL, function (error0, connection) {
            if (error0) {
                throw error0;
            }
            connection.createChannel(function (error1, channel) {
                if (error1) {
                    throw error1;
                }
                var queue = 'socket ID queue';
                channel.assertQueue(queue, {
                    durable: false
                });

                channel.consume(queue, async function (msg) { //Consumes 'msg' from queue

                    let out = JSON.parse(msg.content.toString());
                    let keys = Object.keys(out.data)
                    let arg = []

                    for (let i = 0; i < keys.length; i++) {
                        let key = keys[i];
                        arg.push(out.data[key]);
                    }
                    ++dCounter
                    await eval(out.actions).apply(null, arg).then();
                    console.log(out.actions)
                    resolve()

                });
            });
        });
    });
}

// * use this [uncomment the following for dequeue] *//
/* 
amqp.connect(CONN_URL, async function (error0, connection) {
    
    if (error0) {
        throw error0;
    }
    connection.createChannel(function (error1, channel) {
        if (error1) {
            throw error1;
        }
        var queue = 'socket ID queue';
        channel.assertQueue(queue, {
            durable: false
        });
        dCounter = 0;
       
        channel.consume(queue, async function (msg) { //Consumes 'msg' from queue
            let out = JSON.parse(msg.content.toString());
            let keys = Object.keys(out.data)
            let arg = []

            for (let i = 0; i < keys.length; i++) {
                let key = keys[i];
                arg.push(out.data[key]);
            }
            ++dCounter
            await eval(out.actions).apply(null, arg).then();
            /* let out = JSON.parse(msg.content.toString());
            var funcBody = out.actions.match(/function[^{]+\{([\s\S]*)\}$/);
            console.log(out.data.userId)
            var newFunc = new Function('jsonFunc', funcBody);
            await newFunc(out.data.userId) 

        });
    });
}); */

let purge = function purge() {
    return new Promise((resolve, reject) => {
        amqp.connect(CONN_URL, function (error0, connection) {
            if (error0) {
                throw error0;
            }
            connection.createChannel(function (error1, channel) {
                if (error1) {
                    throw error1;
                }
                var queue = 'socket ID queue';
                channel.assertQueue(queue, {
                    durable: false
                });
                channel.purgeQueue(queue);
                resolve()
            });
        });
    })
}

module.exports = {
    "enqueue": enqueue,
    "dequeue": dequeue,
    purge
}