/*
{
  ! [x] add submission id
  userId: '1',
  userWon: "1",
  gameId: '1',
  roomId: 'room-1',
  action: 'submit',
  languageId: '63',
  questionId: '1',
  socketId: 'Ncr7bB4ozpE5TkMFAAAB',
  testCasesPassed: 0,
  testCasesFailed: 2,
  totalTestCases: 2
}
*/
// let io = require("../../drivers/webserver/server");
let dataFunctions = require("../../data-access/room-db/mongodb/index");
let coinFunctions = require("../coin/index")
const { default: axios } = require("axios");
const config = require('../../config/index');

let coreGameLogic = async (io, data, draw, room) => {
    let submissionDetailsArray = [];
    let minTestCasesPassed = -1;

    for (let i = 0; i < room.socketUserMap.length; i++) {
        if (room.socketUserMap[i].submissions !== undefined) {
            let lastSubmissionId = room.socketUserMap[i].submissions[[room.socketUserMap[i].submissions.length - 1]];
            let submissionDetails = await axios.post(config.microServices.submission + "/submission/get", { submissionId: lastSubmissionId }).then().catch();
            submissionDetails = submissionDetails.data
            submissionDetailsArray.push(submissionDetails);
        }
        else {
            console.log("no submission for user -- ", room.socketUserMap[i].uId)
        }
    }

    if (submissionDetailsArray.length === 0) {

        console.log("No user submissions")
        io.sockets.in(data.roomId).emit('draw', true);
        await coinFunctions.exchangeCoins(room.socketUserMap[0].uId, room.socketUserMap[1].uId, "draw", room.gameId).then().catch()
        draw = true;
        return draw

    } else if (submissionDetailsArray.length === 1) {

        console.log("only one user submitted")

        if (submissionDetailsArray[0].testCasesPassed > 0) {
            data.userWon = submissionDetailsArray[0].userId
            console.log("userWon ", data.userWon)
        }
        else if (submissionDetailsArray[0].testCasesPassed === 0) {
            await coinFunctions.exchangeCoins(room.socketUserMap[0].uId, room.socketUserMap[1].uId, "draw", room.gameId).then().catch()
            io.sockets.in(data.roomId).emit('draw', true);
            console.log("draw")
            draw = true;
            return draw
        }

    } else if (submissionDetailsArray.length === 2) {

        console.log("both user submitted")
        // * coinFunctions.exchangeCoins()
        if (submissionDetailsArray[0].testCasesPassed > submissionDetailsArray[1].testCasesPassed) {
            data.userWon = submissionDetailsArray[0].userId
            console.log("userWon ", data.userWon)

        } else if (submissionDetailsArray[0].testCasesPassed < submissionDetailsArray[1].testCasesPassed) {
            data.userWon = submissionDetailsArray[1].userId
            console.log("userWon ", data.userWon)

        } else {
            await coinFunctions.exchangeCoins(room.socketUserMap[0].uId, room.socketUserMap[1].uId, "draw", room.gameId).then().catch()
            io.sockets.in(data.roomId).emit('draw', true);
            console.log("draw")
            draw = true;
            return draw
        }

    }
    
    let userLost = room.socketUserMap.filter(element => element.uId != data.userWon)
    userLost = userLost[0]
    await dataFunctions.updateUserProps(["_id"], [data.roomId], ["userWon"], [data.userWon]).then().catch();
    await coinFunctions.exchangeCoins(data.userWon, userLost.uId, "won", room.gameId).then().catch()
    io.sockets.in(data.roomId).emit('game', data);
    return false

}

let isPending = async (data) => {
    
    let pending = await axios.post(config.microServices.submission + "/submission/pending", { roomId: data.roomId }).then().catch();
    if (pending.data) {
        return true
    } else return false
}

let decideOutput = async (data, event, io) => {

    let room = await dataFunctions.getRoom(data.roomId).then().catch();

    if (event !== "timeEnded") {
        await dataFunctions.updateUserProps(["_id", "socketUserMap.uId"], [data.roomId, data.userId], ["socketUserMap.$.submissions"], [data.submissionId], "push").then().catch();
    }

    if (room === null) {
        return
    }
    console.log(room.status)
    if (room.status.match(/^GC..$/)) {
        let draw = false
        if (room.status === "GC2" || room.status === "GC4") return
        if (await isPending(data).then().catch()) return

        if (room.status === "GC1P") {
            console.log("No more pending submissions running core game logic...");
            room = await dataFunctions.getRoom(data.roomId).then().catch();
            draw = await coreGameLogic(io, data, draw, room).then().catch()
            await gameEndCleanUp(data.roomId).then().catch()
        }
        return
    }

    if (event === "timeEnded") {
        io.sockets.in(data.roomId).emit('timeEnded', true);
        let draw = false;

        // * logic
        let pending = await axios.post(config.microServices.submission + "/submission/pending", { roomId: data.roomId }).then().catch();
        if (pending.data) {
            
            io.sockets.in(data.roomId).emit('pending', true);
            await dataFunctions.updateUserProps(["_id"], [data.roomId], ["status"], ["GC1P"]).then().catch();
            return
        }
        else {
            draw = await coreGameLogic(io, data, draw, room).then().catch()
        }

        if (draw) {
            await dataFunctions.updateUserProps(["_id"], [data.roomId], ["status"], ["GC3"]).then().catch();
            await gameEndCleanUp(data.roomId).then().catch()
            return
        }
        // * [x] get the recent submission of each user
        // * [x] compare them 
        // * [x] find the best and return
        // * [x] if both people had the same number of testcases try to compare the timestamp else make it a draw.
        //! [check this it doesn't need any action] restrict socketId one per user per game [not rooms within game]
        //! [-] add socket-io redis
        //! [x] add namespaces for different difficulties or on getQuestion aggregate set a match criteria for difficulty
        //! [x] integrate coins
        //! [x] Check GC2
        //! [x] restrict user connect with more than one socket (restrict with userId)

        //! [] check getUserFromHangedUsers
        //! [] handle server restart instead of adding redis
        //! [] handle game reconnection / disconnecting
        await dataFunctions.updateUserProps(["_id"], [data.roomId], ["status"], ["GC1"]).then().catch();
        await gameEndCleanUp(data.roomId).then().catch()
        return
    }

    //! testing remove it
    // data.testCasesPassed = data.totalTestCases
    

    if (data.testCasesPassed === data.totalTestCases) {
        
        // * coinFunctions.exchangeCoins()
        data.userWon = data.userId;
        let userLost = room.socketUserMap.filter(element => element.uId != data.userWon)
        userLost = userLost[0]
        await coinFunctions.exchangeCoins(data.userWon, userLost.uId, "won", room.gameId).then().catch()
        await dataFunctions.updateUserProps(["_id"], [data.roomId], ["status", "userWon"], ["GC2", data.userWon]).then().catch()
        io.sockets.in(data.roomId).emit('game', data);
        await gameEndCleanUp(data.roomId).then().catch()
    }
}

let withdrawEvent = async (data, event, io) => {
    try {

        if (event === "withdrawGame") {

            let room = await dataFunctions.getRoom(data.roomId,"withdraw").then().catch();
            let userWonDetails = room.socketUserMap.filter((element) => element.uId != data.userId)[0];
            let userWon = userWonDetails.uId;
            await dataFunctions.updateUserProps(["_id"], [data.roomId], ["status", "userWon"], ["GC4", userWon]).then().catch();
            let newData = {
                userWon,
                reason: "User chose to withdrew the game"
            }

            // * coinFunctions.exchangeCoins()
            let userLost = room.socketUserMap.filter(element => element.uId != userWon)
            await coinFunctions.exchangeCoins(userWon, userLost.uId, "won", room.gameId).then().catch();

            io.sockets.in(data.roomId).emit('withdrawSuccess', newData);
            console.log("userWon ", userWon)
            await gameEndCleanUp(data.roomId).then().catch()
        }
    }
    catch (e) {
        console.log("user not present")
    }

}




let gameEndCleanUp = async (roomId) => {
    console.log("Game clean up starting...");
    let res = await dataFunctions.getRoom(roomId).then().catch();
    await dataFunctions.addCompletedGame(res.toJSON()).then().catch();
    await dataFunctions.deleteRoomById(roomId).then().catch();
    await dataFunctions.deleteRoomFromHangedUsers(roomId).then().catch();
    console.log("Game clean up Ended");
    // dataFunctions.addCompletedGame()
    // update the room status with time ended.
    // move the room to completedGames.
    // put it on long term storage after 3 days or 10k records and truncate the collection.
}

module.exports = {
    decideOutput,
    withdrawEvent
}