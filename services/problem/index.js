// const problemReq = require('../socket-io/mockProblemAPI');

function allocateProblem() {
    const randomElement = problemReq.totalProblems.problems[Math.floor(Math.random() * problemReq.totalProblems.problems.length)];
    const problem = problemReq.question.filter(it => it._id === randomElement);
    return problem
}

function sendProblemToRoom(res, room, io) {
    io.sockets.in(room).emit('connectToRoom', res);
}

module.exports = {
    allocateProblem,
    sendProblemToRoom
}