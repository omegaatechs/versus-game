
const { default: axios } = require("axios");
let config = require("../../config/index")

let exchangeCoins = async (userWonId, userLostId, event, gameId) => {

    if (event === "draw") {

        for (let k = 0; k < config.otherGameProps.length; k++) {
            if (gameId === parseInt(config.otherGameProps[k].id)) {

                let totalWonAmount = 0;

                let cutCoins = (config.otherGameProps[k].gameDrawPercentageCut / 100) * config.otherGameProps[k].betAmount;
                totalWonAmount = config.otherGameProps[k].betAmount - cutCoins;

                let masterData = {
                    userId: config.microServices.coinsMasterId,
                    coins: cutCoins * 2
                }

                let userLostData = {
                    userId: userLostId,
                    coins: config.otherGameProps[k].betAmount
                };

                let userWonData = {
                    userId: userWonId,
                    coins: config.otherGameProps[k].betAmount
                }
                //! uncomment in prod
                // await axios.post(config.microServices.coins + "/coins/sub", userWonData).then().catch((e) => { console.log("error in coins api") });
                // await axios.post(config.microServices.coins + "/coins/sub", userLostData).then().catch((e) => { console.log("error in coins api") });
                // await axios.post(config.microServices.coins + "/coins/add", masterData).then().catch((e) => { console.log("error in coins api") })
                // userLostData.coins = totalWonAmount;
                // userWonData.coins = totalWonAmount;
                // await axios.post(config.microServices.coins + "/coins/add", userLostData).then().catch((e) => { console.log("error in coins api") })
                // await axios.post(config.microServices.coins + "/coins/add", userWonData).then().catch((e) => { console.log("error in coins api") })
                return
            }
        }
    }

    for (let k = 0; k < config.otherGameProps.length; k++) {

        if (gameId === parseInt(config.otherGameProps[k].id)) {

            let totalWonAmount = 0;

            let cutCoins = (config.otherGameProps[k].percentageCut / 100) * config.otherGameProps[k].betAmount;
            totalWonAmount = config.otherGameProps[k].betAmount - cutCoins;
            console.log(cutCoins)
            let userLostData = {
                userId: userLostId,
                coins: config.otherGameProps[k].betAmount
            };

            let masterData = {
                userId: config.microServices.coinsMasterId,
                coins: cutCoins
            }

            let userWonData = {
                userId: userWonId,
                coins: totalWonAmount.toString()
            }
            //! uncomment in prod
            // await axios.post(config.microServices.coins + "/coins/sub", userLostData).then().catch((e) => { console.log("error in coins api") });
            // await axios.post(config.microServices.coins + "/coins/add", masterData).then().catch((e) => { console.log("error in coins api") })
            // await axios.post(config.microServices.coins + "/coins/add", userWonData).then().catch((e) => { console.log("error in coins api") })
            return
        }
    }
}

module.exports = {
    exchangeCoins
}