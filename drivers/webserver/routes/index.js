const express = require('express')
const router = express.Router()
const marked = require("marked");
const fsp = require('fs').promises;
const gameLogic = require("../../../services/game-logic/index")
let socketIO = null;
const dataFunctions = require('../../../data-access/room-db/mongodb/index')

router
  .post("/submission", async (req, res) => {
    res.send("ok")
    await gameLogic.decideOutput(req.body, "submission", socketIO).then().catch()
  })

  .post("/time", async (req, res) => {
    res.send("ok");
    await gameLogic.decideOutput(req.body, "timeEnded", socketIO).then().catch()
  })

  .post("/user/hanged", async (req, res) => {
    let result = await dataFunctions.getUserFromHangedUsers(req.body.userId).then().catch()
    res.send(result !== null);
  })

  .post("/games/completed", async (req, res) => {
    // let result = await dataFunctions.getGamesByUser(req.body.userId).then().catch()

    res.send(await dataFunctions.getGamesByUser(req.body.userId).then().catch());
  })

router

  .get("/", async (req, res) => {
    // let result = await dataFunctions.getGamesByUser(req.body.userId).then().catch()

    res.send("Welcome to game microservice API");
  })

  .get("/game/status", async (req, res) => {
    // let result = await dataFunctions.getGamesByUser(req.body.userId).then().catch()

    res.send("healthy");
  })

router
  .post("/game/submission", async (req, res) => {
    res.send("ok")
    await gameLogic.decideOutput(req.body, "submission", socketIO).then().catch()
  })

  .post("/game/time", async (req, res) => {
    res.send("ok");
    await gameLogic.decideOutput(req.body, "timeEnded", socketIO).then().catch()
  })

  .post("/game/user/hanged", async (req, res) => {
    let result = await dataFunctions.getUserFromHangedUsers(req.body.userId).then().catch()
    res.send(result !== null);
  })

  .post("/game/games/completed", async (req, res) => {
    // let result = await dataFunctions.getGamesByUser(req.body.userId).then().catch()

    res.send(await dataFunctions.getGamesByUser(req.body.userId).then().catch());
  })




module.exports = function (io) {
  socketIO = io;
  return router
}

/*
router
  .get("/", async (req, res) => {
    let data = await fsp.readFile("C:/Users/Khiruparaj/Documents/0_Gigacode/versus-game/services/socket-io/room-status.md").then().catch();
    res.render(marked(data.toString()))
  });

  .get("/", async (req, res) => {
    let data = await fsp.readFile("C:/Users/Khiruparaj/Documents/0_Gigacode/versus-game/services/socket-io/room-status.md").then().catch();
    res.render(marked(data.toString()))
  });

  const renderer = {
  heading(text, level) {
    const escapedText = text.toLowerCase().replace(/[^\w]+/g, '-');

    return `
            <head>
                <script src="https://cdn.jsdelivr.net/npm/marked/marked.min.js"></script>
            </head>
            ${text}
            `;
    }
  };

  marked.use({ renderer });

*/