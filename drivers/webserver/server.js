const express = require('express');
const cors = require('cors');
const app = express();
const server = require('http').createServer(app);
const bodyParser = require('body-parser').json();
const config = require("../../config")
const PORT = config.PORT;
const io = require('socket.io')(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"]
  }
});

// let dataFunctionsHanged = require("../../data-access/room-db/mongodb/hangedusers")

app.io = io;
/* 
const redisAdapter = require('socket.io-redis');
io.adapter(redisAdapter({ host: 'localhost', port: 6381, db:0 }));
*/
const socket = require("../../services/socket-io/socket");
socket(io)

const routes = require("./routes")(io);

// ! remove in prod
const dataFunctions = require("../../data-access/room-db/mongodb/index");
const { default: axios } = require('axios');

// @app use cors to allow cross origin resource sharing
app.use(
  cors({
    origin: "*",
    credentials: true,
  })
);


//Set view engine to ejs
app.set("view engine", "ejs");
app.set("views", __dirname + "/views");


app.use(bodyParser);
app.use(routes)

// === BOILERPLATE ===
// Catch and send error messages
app.use((err, req, res, next) => {
  if (err) {
    console.error(err.message)
    if (!err.statusCode) {
      err.statusCode = 500
    } // Set 500 server code error if statusCode not set
    return res.status(err.statusCode).send({
      statusCode: err.statusCode,
      message: err.message
    })
  }
  next()
})

// 404
app.use(function (req, res) {
  res.status(404).json({
    status: 'Page does not exist'
  });
});

server.listen(PORT, async () => {

  console.log(`Listening on PORT: ${PORT}\n`);
  // console.log(`TODO:\n 
  // [] Check for a final time and upload\n
  // [] don't forget to remove truncate call to db\n` )
  
  // await axios.get("http://gigacode-alb-771475205.ap-south-1.elb.amazonaws.com/coins/status")
  // .then((res)=>console.log("Coin microservice works", res.data))
  // .catch(err => console.log("error in coins ", err))

  // await axios.get("http://gigacode-alb-771475205.ap-south-1.elb.amazonaws.com/payments/status")
  // .then((res)=>console.log("Payment microservice works", res.data))
  // .catch(err => console.log("error in payment ", err))

  // await axios.get("http://gigacode-alb-771475205.ap-south-1.elb.amazonaws.com:3000/")
  // .then((res)=>console.log("Submission microservice works", res.data))
  // .catch(err => console.log("error in Submission ", err))

  // await axios.get(config.microServices.submission + "/").then((res)=>{console.log(res.data)}).catch()
  await axios.get(config.microServices.submission + "/truncate/db").then().catch()
  await dataFunctions.deleteAllHangedUsers();
  await dataFunctions.deleteAllRooms();
  await dataFunctions.deleteAllCompletedGames();
  //! add cron job
});

module.exports = {
  io
}