# Table

| Status code   | Meaning       |
| ------------- |-------------|
| RC            | Empty Room created              |
| RCO           | Room Created with One user      |
| RF            | Room Full |
| WFU           | Waiting for users [0 users in room prev enter (after RC)] |
| HU            | Hanged User [on or more users left but some users are still in room ] |
| AUL           | All users left [network problem - time still available] |
| GO            | Game Ongoing |
| TRO           | Time ran out |
| GC1           | Game Completed due to time ran out |
| GC1P          | Game Completed due to time ran out pending for submissions by user|
| GC2           | Game Completed because of one user passed all testcase|
| GC3           | Game Completed by draw |
| GC4           | Game Completed because one user withdrew the game|
