let Room = require('../../../db/mongodb/models/rooms')
let hangedUser = require('../../../db/mongodb/models/hangedUser')
let makeRoom = require('../../../models/rooms/index')
let serialize = require('./serializer')
let config = require('../../../config')
const roomFunctions = require('./rooms')

let getUserFromHangedUsers = async (uId) => {
  return await hangedUser.findOne({ "_id": uId }).then().catch()
}

let removeUserFromHangedUsers = async (uId) => {
  return await hangedUser.findByIdAndDelete(uId).then().catch()
}

let deleteAllHangedUsers = async () => {
  return await hangedUser.deleteMany({}).then().catch()
}

// ** User specific functions **
let getRoom = async (roomId, event) => {
  return await Room.findById(roomId).then().catch()
}

let addUserToRoom = async (roomId, socketId, uId, event) => {

  if (event === "resumeGame") {
    await removeUserFromHangedUsers(uId).then().catch();
  }

  let res = await roomFunctions.UsersInRoom(roomId).then().catch(err => { return err })
  if (res.length < config.game.room.MAX_USERS) {
    return await Room.updateOne({ "_id": roomId }, { $push: { "socketUserMap": { "_id": socketId, "uId": uId, "status": "N" } } })
      .then()
      .catch()
  }

}

let updateUserProps = async (findProps, findValues, props, values, op) => {

  if (findProps.length !== findValues.length || props.length !== values.length) return new Error("props and value did not match")
  let findConstructor = {}, updateConstructor = {}
  for (let i = 0; i < findProps.length; i++) {
    findConstructor[findProps[i]] = findValues[i]
  }

  for (let j = 0; j < props.length; j++) {
    updateConstructor[props[j]] = values[j]
  }

  if (op === "push") {
    return await Room.updateOne(findConstructor, { $push: updateConstructor }).then().catch();
  }

  return await Room.updateOne(findConstructor, { $set: updateConstructor }).then().catch();

}


let addHangedUser = async (hangedUserEntry) => {
  return await hangedUser.create(hangedUserEntry).then().catch();
}

let deleteFromHangedUsers = async (uId) => {
  return await hangedUser.deleteOne({ "_id": uId }).then().catch();
}


let deleteRoomFromHangedUsers = async (roomId) => {
  return await hangedUser.deleteMany({ "prevRoom": roomId }).then().catch();
}


let CreateAndAddHangedUser = async (roomId, socketId, event) => {
  let result = await Room.findOne({ "_id": roomId }).then().catch()

  let hangedUserEntry = {}

  if (event === "disconnecting") {
    if (result === null) {
      console.log("Room does not exists. Possible scenario: It could be cleaned up or user disconnected because of lack of coins")
      return
    }
    try {
      let hangedUserDetail = await result.socketUserMap.filter((item) => item._id === socketId)[0];
      hangedUserEntry = {};
      if (hangedUserDetail) {
        hangedUserEntry['_id'] = hangedUserDetail.uId; hangedUserEntry['prevRoom'] = roomId;
        return await hangedUser.create(hangedUserEntry).then().catch();
      }
    }
    catch (e) {
      console.log("error while disconnecting event")
    }

  }
}

let shutDownHandler = async () => {

  console.log("hello")
  let roomRegex = 'room-' + config.serviceInstanceId + '.*';
  roomRegex = new RegExp(roomRegex, "i");
  console.log(roomRegex);

  return Room.find({ "_id": { $regex: roomRegex } }).then(async (result) => {

    for (let i = 0; i < result.length; i++) {
      let socketUserMapArray = result[i].socketUserMap;
      hangedUserEntry = {};
      for (let j = 0; i < socketUserMapArray.length; j++) {
        let hangedUserDetail = socketUserMapArray[j]
        hangedUserEntry['_id'] = hangedUserDetail.uId; hangedUserEntry['prevRoom'] = result._id;
        return await hangedUser.create(hangedUserEntry).then().catch();
      }
    }

  }).catch()
}

let checkUserInHangedUser = async () => {
  return await hangedUser.findOne().then().catch()
}

let updateUserStatus = async (roomId, socketId, event) => {
  let result = await Room.findOne({ "_id": roomId }).then().catch()
  if (event === "disconnecting") {
    let hangedUserDetail = await result.socketUserMap.filter((item) => item.id === socketId)[0];
    let hangedUserEntry = {};
    hangedUserEntry['_id'] = hangedUserDetail.uId; hangedUserEntry['prevRoom'] = roomId;
    await addHangedUser(hangedUserEntry).catch(err => { throw new Error(`User already exists in hanged user`) })
  }

  return await Room.updateOne({ "_id": id, "socketUserMap._id": socketId },
    { $set: { "socketUserMap.$._id": "", "socketUserMap.$.status": "H" } }).then().catch()
}

let removeUserFromRoom = async (roomId, socketId, event) => {

  let result = await Room.findOne({ "_id": roomId }).then().catch()
  console.log("removeUserFromRoom result ", result)

  if (event === "disconnecting") {

    console.log("removeUserFromRoom disconnecting result", result);
    let hangedUserDetail = await result.socketUserMap.filter((item) => item.id === socketId)[0];
    let hangedUserEntry = {};
    hangedUserEntry['_id'] = hangedUserDetail.uId; hangedUserEntry['prevRoom'] = roomId;
    await addHangedUser(hangedUserEntry).catch(err => { throw new Error(`User already exists in hanged user`) })

  }

  result.socketUserMap = result.socketUserMap.filter((item) => item.id !== socketId);
  return await Room.updateOne({ "_id": id }, result).then().catch()

}

module.exports = {
  addUserToRoom,
  removeUserFromRoom,
  getUserFromHangedUsers,
  removeUserFromHangedUsers,
  deleteAllHangedUsers,
  updateUserProps,
  addHangedUser,
  CreateAndAddHangedUser,
  deleteFromHangedUsers,
  getRoom,
  shutDownHandler,
  deleteRoomFromHangedUsers
}