let {
  createRoom,
  deleteRoomById,
  deleteAllRooms,
  listRooms,
  UsersInRoom,
  doesUserExitsInGame,
  isRoomEmpty,
  roomExists
} = require('./rooms')

let {
  addUserToRoom,
  removeUserFromRoom,
  getUserFromHangedUsers,
  removeUserFromHangedUsers,
  deleteAllHangedUsers,
  updateUserProps,
  addHangedUser,
  CreateAndAddHangedUser,
  deleteFromHangedUsers,
  getRoom,
  deleteRoomFromHangedUsers
} = require('./hangedusers')

let {
  addCompletedGame,
  deleteAllCompletedGames,
  getCompletedGameById,
  getGamesByUser
} = require('./completedGames')

module.exports = {
  createRoom,
  addUserToRoom,
  deleteRoomById,
  deleteAllRooms,
  removeUserFromRoom,
  getUserFromHangedUsers,
  removeUserFromHangedUsers,
  deleteAllHangedUsers,
  listRooms,
  doesUserExitsInGame,
  updateUserProps,
  addHangedUser,
  CreateAndAddHangedUser,
  deleteFromHangedUsers,
  getRoom,
  UsersInRoom,
  addCompletedGame,
  deleteAllCompletedGames,
  getCompletedGameById,
  getGamesByUser,
  deleteRoomFromHangedUsers
}