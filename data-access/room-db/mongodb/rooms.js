let Room = require('../../../db/mongodb/models/rooms')
let hangedUser = require('../../../db/mongodb/models/hangedUser')
let makeRoom = require('../../../models/rooms/index')
let serialize = require('./serializer')
let config = require('../../../config')

// ** Room Specific functions **

let createRoom = (roomId, socketUserMap, gameId, status) => {

    let roomInfo = {
        roomId,
        socketUserMap,
        gameId,
        status
    }
    let room = makeRoom(roomInfo)
    return Room.create(serialize.serializerForDb(room)).then().catch()

}

let deleteRoomById = async (roomId) => {
    return await Room.deleteOne({ "_id": roomId }).then().catch()
}

let deleteAllRooms = async () => {
    return await Room.deleteMany({}).then().catch()
}

let listRooms = async () => {

    console.log("inside listRooms")
    return Room.find({})
        .then(serialize.serializer)
        .catch()
}

let findRoomById = async (prop, val) => {

    if (prop === 'roomId') {
        prop = '_id'
    }
    return await Room.find({ [prop]: val })
        .then(resp => {

            return serialize.serializer(resp[0])
        })
}

let UsersInRoom = async (roomId) => {

    let res = await findRoomById("roomId", roomId).then().catch()
    if (res !== null && Array.isArray(res.socketUserMap)) {
        return res.socketUserMap
    } else {
        return new Error("room not found")
    };

}

let isRoomEmpty = async (roomId) => {

    let res = await findRoomById("roomId", roomId).then().catch()
    if (res !== null && Array.isArray(res.socketUserMap)) {
        return res.socketUserMap.length === 0
    } else {
        return new Error("room not found")
    };

}

let roomExists = async (roomId) => {
    return Room.find({ "roomId": roomId })
        .then(resp => {
            if (resp === null) return false
            else true
        })
}

// **check if a user is already in a room**
let userExistsInARoom = async (roomId, uId) => {

    let res = await UsersInRoom(roomId);
    return res.filter((item) => item.uId === uId).length === 1
}


let doesUserExitsInGame = async (userId) => {
    let res = await Room.find({ "socketUserMap.uId": userId }).then().catch();
    return res
}

module.exports = {
    createRoom,
    deleteRoomById,
    deleteAllRooms,
    listRooms,
    UsersInRoom,
    isRoomEmpty,
    roomExists,
    doesUserExitsInGame
}