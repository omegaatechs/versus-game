let completedGames = require('../../../db/mongodb/models/completedGames');

// ** Room Specific functions **

let addCompletedGame = (room) => {
    return completedGames.create(room).then().catch()
}

let getCompletedGameById = (roomId) => {
    return completedGames.findById(roomId).then().catch()
}

let deleteAllCompletedGames = async () => {
    return await completedGames.deleteMany({}).then().catch()
}

let getGamesByUser = async (userId) => {
    return await completedGames.find({ "socketUserMap": { $elemMatch: { uId: userId } } }, { "_id": false, "socketUserMap": false, "__v": false, "problemId": false, "time": false }).lean().then().catch()
    // return await completedGames.find({ "socketUserMap.$.uId": userId }).then().catch()
}

module.exports = {
    addCompletedGame,
    deleteAllCompletedGames,
    getCompletedGameById,
    getGamesByUser
}