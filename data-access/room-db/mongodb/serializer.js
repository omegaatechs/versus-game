const _serializeSingle = (room) => {
    return {
        'roomId': room._id,
        'socketUserMap': room.socketUserMap,
        'gameId': room.gameId,
        'status': room.status
    };
};

const _serializeSingleForDb = (room) => {
    return {
        '_id': room.roomId,
        'socketUserMap': room.socketUserMap,
        'gameId': room.gameId,
        'status': room.status
    };
};

const serializer = (data) => {
    if (!data) {
        return null
    }
    if (Array.isArray(data)) {
        return data.map(_serializeSingle)
    }
    return _serializeSingle(data)
}

const serializerForDb = (data) => {
    if (!data) {
        return null
    }
    if (Array.isArray(data)) {
        return data.map(_serializeSingleForDb)
    }
    return _serializeSingleForDb(data)
}

module.exports = {serializer, serializerForDb}